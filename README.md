# JS + Angular test - Dots

## Installation

* Install NodeJS and npm.
* Install Angular CLI : `npm install -g @angular/cli`. This command may need to be run as admin (using `sudo` on Linux / MacOS or administrator mode on Windows).
* In this directory, run `npm install`.

## Development

* Start the local development server using `ng serve`.
* You should now be able to access the website at `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

