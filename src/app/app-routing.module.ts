import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DotsComponent } from './dots/dots.component';

const routes: Routes = [
	{ path: 'dots', component: DotsComponent },
	{ path: '**', redirectTo: 'dots' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
