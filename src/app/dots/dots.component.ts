import { Component, OnInit } from '@angular/core';

interface Dot {
	x: number,
	y: number,
	color: string
}

const DOTS: Dot[] = [
	{ x: 100, y: 100, color: 'darkblue' },
	{ x: 100, y: 500, color: 'red' },
	{ x: 500, y: 100, color: 'green' },
	{ x: 500, y: 500, color: 'darkred' },

	{ x: 300, y: 300, color: 'pink' },

	{ x: 300, y: 200, color: 'blue' },
	{ x: 300, y: 400, color: 'blue' },
	{ x: 200, y: 300, color: 'blue' },
	{ x: 400, y: 300, color: 'blue' },
	{ x: 350, y: 250, color: 'blue' },
	{ x: 350, y: 350, color: 'blue' },
	{ x: 250, y: 350, color: 'blue' },
	{ x: 250, y: 250, color: 'blue' }
];

@Component({
	selector: 'app-dots',
	templateUrl: './dots.component.html',
	styleUrls: ['./dots.component.scss']
})
export class DotsComponent implements OnInit {

	dots: Dot[];

	redRectangleDimensions: {
		x: number,
		y: number,
		width: number,
		height: number
	};

	constructor() { }

	ngOnInit() {
		this.moveRedRectangle();
	}

	moveRedRectangle() {

		this.redRectangleDimensions = {
			x: (20 * Math.random()) * window.innerWidth / 100,
			y: (20 * Math.random()) * window.innerHeight / 100,
			width: (20 + 60 * Math.random()) * window.innerWidth / 100,
			height: (20 + 60 * Math.random()) * window.innerHeight / 100
		}

		this.dots = this.convertCoordinates(DOTS);
	}



	/**
	 * Should return the coordinates (relative to the black rectangle)
	 * so that all the dots fit the red rectangle.
	 */
	private convertCoordinates(entryDots: Dot[]): Dot[] {
		let dots = JSON.parse(JSON.stringify(entryDots));

		/**
		 * Write your code here
		 */

		return dots;
	}

  changeColor(dot: Dot) {
    console.log('Clicked dot', dot);
  }

}
